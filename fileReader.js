var FindFiles = require('node-find-files');
var _ = require('underscore');
var request = require('request');

var togglZapierURL = 'https://zapier.com/hooks/catch/{{ZAPIER_ZAP_API}}';

var projects = [
	{
		'projectName': 'Task Recording Test',
		'rootPath': 'testFiles',
		'start': 0,
		'end': 0,
		'count': 0,
		'projectID': 1,
		'createdWith': 'Username'
	}
];

var run = function() {
	var interval = 300000; //5 minutes in milliseconds
	var curTime = new Date().getTime() - interval;

	var processResults = function(results) {

		_.map(projects, function(proj, key) {

			if (proj.projectName === results.projectName) {

				//Send to Toggl API
				if (results.end <= 0 && proj.start > 0 && proj.end > 0) {
					console.log('No files updated - send to API here');

					var dataToSend = {
						'projectID': proj.projectID,
						'createdWith': proj.createdWith,
						'start': proj.start,
						'duration': Math.round((proj.end - proj.start) / 1000)
					};

					request.post(
						togglZapierURL,
						{json :dataToSend},
						function (error, response, body) {
							if (!error && response.statusCode == 200) {
								proj.start = 0;
								proj.end = 0;
								console.log('New recording submitted');
								console.log(dataToSend);
							}
						}
						);

					//Further changes have been made so update the end timestamp
				} else if (results.end > 0 && results.count > 0) {

					//New recording of this task so set the start timestamp
					if (results.start > 0) {
						proj.start = results.start;
					}
					proj.end = results.end;
					proj.count++;
					console.log(proj.projectName+" updated");
				} else {
					console.log('No Updates');
				}
			}
			return proj;
		});
	}

	_.each(projects, function(project) {

		var results = {
			'projectName': project.projectName,
			'count': 0,
			'start': (project.start === 0) ? curTime : project.start,
			'end': 0
		};

		var finder = new FindFiles({
			rootFolder : project.rootPath,
			filterFunction : function (path, stat) {
				return (stat.mtime > curTime) ? true : false;
			}
		});

		finder.on("match", function(strPath, stat) {
			if (stat.mtime.getTime() > results.end) {
				results.end = stat.mtime.getTime();
			}
			results.count++;
		});

		finder.on("complete", function() {
			processResults(results);
		});

		finder.startSearch();
	});
}

run();
setInterval(run, 10000);
